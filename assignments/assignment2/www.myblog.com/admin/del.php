<?php 
  require("../classes/auth.php");
  require("header.php");
  require("../classes/db.php");
  require("../classes/phpfix.php");
  require("../classes/post.php");
  $rand = bin2hex(openssl_random_pseudo_bytes(16)); # generate rand
  $_SESSION["nocsrftoken"] = $rand;
?>

<?php  
  $token = $_GET["nocsrftoken"]; ##### GET NOT POST
  echo $token;
  if (!isset($token) or ($token!=$_SESSION["nocsrftoken"])){ # like in index, compare the token here with the session token
    echo "test";
    echo "CSRF Attack is detected (del.php)";
    die();
  }

  $post = Post::delete((int)($_GET["id"]));
  header("Location: /admin/index.php");
?>

