<?php

class Post{
  public $id, $title, $text, $published;
  function __construct($id, $title, $text, $published){
    $this->title= $title;
    $this->text = $text;
    $this->published= $published;
    $this->id = $id;
  }   

 
  function all($cat=NULL,$order =NULL) {
    /* this query does not use user inputs, so you do not need to implement prepared statements */
    global $dblink;
    $sql = "SELECT * FROM posts";
    if (isset($order)) 
      $sql .= "order by ".mysqli_real_escape_string($dblink, $order);  
    $results= mysqli_query($dblink, $sql);
    $posts = Array();
    if ($results) {
      while ($row = mysqli_fetch_assoc($results)) {
        $posts[] = new Post($row['id'],$row['title'],$row['text'],$row['published']);
      }
    }
    else {
      echo mysqli_error($dblink);
    }
    return $posts;
  }
 

  function render_all($pics) {
    /* this query does not use text user inputs, so you do not need to implement prepared statements */
    echo "<ul>\n";
    foreach ($pics as $pic) {
      echo "\t<li>".$pic->render()."</a></li>\n";
    }
    echo "</ul>\n";
  }

  /* this query does not use user inputs, so you do not need to implement prepared statements */
  function render_edit() {
    $str = "<img src=\"uploads/".h($this->img)."\" alt=\"".h($this->title)."\" />";
    return $str;
  } 
  
  /* this query does not use user inputs, so you do not need to implement prepared statements */
  function render() {
    $str = "<h2 class=\"title\"><a href=\"/post.php?id=".h($this->id)."\">".h($this->title)."</a></h2>";
    $str.= '<div class="inner" align="center">';
    $str.= "<p>".htmlentities($this->text)."</p></div>";   
    $str.= "<p><a href=\"/post.php?id=".h($this->id)."\">";
    $count = htmlentities($this->get_comments_count());
    switch ($count) {
    case 0:
        $str.= "Be the first to comment";
        break;
    case 1:
        $str.= "1 comment";
        break;
    case 2:
        $str.= $count." comments";
        break;
    }    
    $str.= "</a></p>";
    return $str;
  }
   /* used prepared statements here because it takes in id as an input--could be modified by a user */
  function add_comment() {
    global $dblink;
    $sql  = "INSERT INTO comments (title,author, text, post_id) values ('";
    $sql .= mysqli_real_escape_string($dblink, htmlspecialchars($_POST["title"]))."','";
    $sql .= mysqli_real_escape_string($dblink, htmlspecialchars($_POST["author"]))."','";
    $sql .= mysqli_real_escape_string($dblink, htmlspecialchars($_POST["text"]))."',";
    $sql .= intval($this->id).")";
    $result = mysqli_query($dblink, $sql);
    echo mysqli_error(); 
  } 
   /* used prepared statements here becuase it takes in id as an input--could be modified by a user */
  function render_with_comments() {
    $str = "<h2 class=\"title\"><a href=\"/post.php?id=".h($this->id)."\">".h($this->title)."</a></h2>";
    $str.= '<div class="inner" style="padding-left: 40px;">';
    $str.= "<p>".htmlentities($this->text)."</p></div>";   
    $str.= "\n\n<div class='comments'><h3>Comments: </h3>\n<ul>";
    foreach ($this->get_comments() as $comment) {
      $str.= "\n\t<li>".htmlentities($comment->text)."</li>"; #use htmlentities to sanitize input
    }
    $str.= "\n</ul></div>";
    return $str;
  }

  /* this query does not use user inputs, so you do not need to implement prepared statements */
  function get_comments_count() {
    global $dblink;
    if (!preg_match('/^[0-9]+$/', $this->id)) {
      die("ERROR: INTEGER REQUIRED");
    }
    $comments = Array();
    $result = mysqli_query($dblink, "SELECT count(*) as count FROM comments where post_id=".$this->id);
    $row = mysqli_fetch_assoc($result);
    return $row['count'];
  } 
 
  function get_comments() {
  /* this query does not use user inputs, so you do not need to implement prepared statements */
    global $dblink;
    if (!preg_match('/^[0-9]+$/', $this->id)) {
      die("ERROR: INTEGER REQUIRED");
    }
    $comments = Array();
    $results = mysqli_query($dblink, "SELECT * FROM comments where post_id=".$this->id);
    if (isset($results)){
      while ($row = mysqli_fetch_assoc($results)) {
        $comments[] = Comment::from_row($row);
      }
    }
    return $comments;
  } 

  /* implemented prepared statements here */
  function find($id) {
    global $dblink;
    /*$result = mysqli_query($dblink, "SELECT * FROM posts where id=".$id);
    $row = mysqli_fetch_assoc($result); 
    if (isset($row)){
      $post = new Post($row['id'],$row['title'],$row['text'],$row['published']);
    }*/

    //new code for SQLi prevention
    $prepared_sql = "SELECT id, title, text, published FROM posts WHERE id=?";
    $stmt = mysqli_prepare($dblink, $prepared_sql);
    mysqli_stmt_bind_param($stmt, 'i', $id);
    mysqli_stmt_execute($stmt);
    // bind result variables
    mysqli_stmt_bind_result($stmt, $id, $title, $text, $published);
    if(mysqli_stmt_fetch($stmt)){
      // use 'while (mysqli_stmt_fetch($stmt)) {...} if there are multiple rows'
      echo "Debug>result: $id, $title, $text, $published";
      $post = new Post($id, $title, $text, $published);
    } else{
      echo "Debug: Invalid input or SQLi prevented!";
      die();
    }
    //end new code
    return $post;
  }

  /* used prepared statements here becuase it takes in id as an input--could be modified by a user */
  function delete($id) {
    global $dblink;
    if (!preg_match('/^[0-9]+$/', $id)) {
      die("ERROR: INTEGER REQUIRED");
    }
    $prepared_sql = "DELETE FROM posts where id=?";
    $stmt = mysqli_prepare($dblink, $prepared_sql);
    mysqli_stmt_bind_param($stmt, 'i', $id);

    mysqli_stmt_execute($stmt);

    // bind result variables
    mysqli_stmt_bind_result($stmt, $id, $title, $text, $published);

    if(mysqli_stmt_fetch($stmt)){
    // use 'while (mysqli_stmt_fetch($stmt)) {...} if there are multiple rows'
      echo "Debug>delete result: $id, $title, $text, $published";
      $post = new Post($id, $title, $text, $published);
    } else{
      echo "Debug: Invalid input or SQLi prevented!";
      die();
  } // end new code
    // $result = mysqli_query($dblink, "DELETE FROM posts where id=".(int)$id);
  }
  
  /* I tried implementing prepared statements here but it would always break the website--however, you don't need them because inputs are escaped by the mysqli_real_escape_string*/
  function update($title, $text) {
      global $dblink;

      // $prepared_sql = "UPDATE posts SET title=?, text=? WHERE id=?";
      // $stmt = mysqli_prepare($dblink, $prepared_sql);
      // mysqli_stmt_bind_param($stmt, 'i', $id);
      // mysqli_stmt_bind_param($stmt, 's', $title);
      // mysqli_stmt_bind_param($stmt, 's', $text);

      // mysqli_stmt_execute($stmt);
      // // bind result variables
      // mysqli_stmt_bind_result($stmt, $id, $title, $text, $published);

      // while(mysqli_stmt_fetch($stmt)){
      //       // use 'while (mysqli_stmt_fetch($stmt)) {...} if there are multiple rows'
      //       echo "Debug>result: this.$id, this.$title, this.$text, this.$published";
      //       $post = new Post($this.id, $this.title, $this.text, $this.published);
      //     } else{
      //       echo "Debug: Invalid input or SQLi prevented!";
      //       die();
      //   }


      $sql = "UPDATE posts SET title='";
      $sql .= mysqli_real_escape_string($dblink, htmlspecialchars($_POST["title"]))."',text='";
      $sql .= mysqli_real_escape_string($dblink, htmlspecialchars($_POST["text"]))."' WHERE id=";
      $sql .= intval($this->id);
      $result = mysqli_query($dblink,$sql);
      $this->title = $title; 
      $this->text = $text; 
  } 

  /* this query does not use user inputs, so you do not need to implement prepared statements */
  function create(){
      global $dblink;
      $sql = "INSERT INTO posts (title, text) VALUES ('";
      $title = mysqli_real_escape_string($dblink, htmlspecialchars($_POST["title"]));
      $text = mysqli_real_escape_string($dblink, htmlspecialchars($_POST["text"]));
      $sql .= $title."','".$text;
      $sql.= "')";
      $result = mysqli_query($dblink,$sql);

  }
}
?>
